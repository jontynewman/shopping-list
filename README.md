# JontyNewman\ShoppingList

[![Build Status](https://travis-ci.org/JontyNewman/shopping-list.svg?branch=master)](https://travis-ci.org/JontyNewman/shopping-list)

An application for generating shopping (grocery) lists based on specified meals.

## Quick Start

```
composer create-project --no-dev 'jontynewman/shopping-list ^1.0'
cd shopping-list
php -S localhost:8000 index.php
```

## Configuration

By default, the application will make use of an XML file in order to determine
the necessary ingredients for the specified meals. Its format should resemble
the `meals.xml` file provided.

To use another method for persisting data (e.g. an SQL database), a custom
`\JontyNewman\ShoppingList\Repository` can be implemented.

## Execution

By default, the project runs as a web application using
[ShrooPHP\Framework](https://github.com/shroophp/framework) and a `meals.xml`
file that can be found via the include path. This behavior is defined in
`index.php`.

For advanced execution methods (e.g. for a production server), see
<https://github.com/shroophp/framework#configuration>

To execute the project in another manner (e.g. using a different XML file,
repository or user interface), `index.php` can be modified.
