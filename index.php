<?php

use JontyNewman\ShoppingList\Applications\WebApplication;
use JontyNewman\ShoppingList\Presenters\HtmlPresenter;
use JontyNewman\ShoppingList\Repositories\XmlFileRepository;

require 'vendor/autoload.php';

$routes = array(
	      WebApplication::MEALS => WebApplication::MEALS_PATH,
	WebApplication::INGREDIENTS => WebApplication::INGREDIENTS_PATH,
	   WebApplication::ITEMIZED => WebApplication::ITEMIZED_PATH,
);

$presenter = new HtmlPresenter(
	$routes[WebApplication::MEALS],
	$routes[WebApplication::INGREDIENTS],
	$routes[WebApplication::ITEMIZED]
);

$app = new WebApplication;
$app->run(new XmlFileRepository('meals.xml'), $presenter, $routes);
