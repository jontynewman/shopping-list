<form action="<?= htmlentities($this->ingredients); ?>" method="post">
	<?php foreach ($meals as $meal): ?>
	<p class="field">
		<label>
			<input type="checkbox" name="meals[]" value="<?= htmlentities($meal); ?>">
			<?= htmlentities($meal); ?>
		</label>
	</p>
	<?php endforeach; ?>
	<p class="center">
		<input type="submit" value="Generate Shopping List">
	</p>
</form>
