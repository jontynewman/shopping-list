<?php $i = 0; ?>
<p>
	<a href="<?= htmlentities($this->meals); ?>">Go Back</a>
</p>
<form action="<?= htmlentities($this->itemized); ?>" method="post">
	<ul>
		<?php foreach ($ingredients as $ingredient => $meals): ?>
		<li>
			<input type="checkbox"
					name="ingredients[<?= htmlentities($ingredient); ?>]"
					value="<?= htmlentities(count($meals)); ?>"
					checked="checked"
			/>
			<a href="#ingredient<?= htmlentities($i); ?>">
				<?= htmlentities(count($meals)); ?>
				&times;
				<?= htmlentities($ingredient); ?>
			</a>

			<ul id="ingredient<?= htmlentities($i); ?>">
				<?php foreach ($meals as $meal): ?>
				<li><?= htmlentities($meal); ?></li>
				<?php endforeach; ?>
			</ul>
		</li>
		<?php $i++; endforeach; ?>
	</ul>
	<p class="center">
		<input type="submit" value="Download">
	</p>
</form>
<p>
	<a href="<?= htmlentities($this->meals); ?>">Go Back</a>
</p>
