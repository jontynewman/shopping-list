<!DOCTYPE html>
<html>
	<head>
		<title>Shopping List</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<style type="text/css">

			body {
				font-family: sans-serif;
				margin-left: auto;
				margin-right: auto;
				max-width: 24rem;
			}

			ul[id^=ingredient] {
				display: none;
			}

			ul[id^=ingredient]:target {
				display: block;
			}

			form > p.center,
			:not(form) > p {
				text-align: center;
			}

		</style>

		<?php call_user_func($inset); ?>

	</body>
</html>
