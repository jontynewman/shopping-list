<?php

namespace JontyNewman\ShoppingList\Presenters;

use JontyNewman\ShoppingList\Presenter;

/**
 * A presenter of various HTML views within the application.
 */
class HtmlPresenter implements Presenter
{
	/**
	 * @var string the path to meals
	 */
	private $meals;

	/**
	 * @var string the path to ingredients for given meals
	 */
	private $ingredients;

	/**
	 * @var string the path to ingredients as an itemized list
	 */
	private $itemized;

	/**
	 * Constructs a presenter of various HTML views within the application.
	 *
	 * @param string $meals the path to meals
	 * @param string $ingredients the path to ingredients for given meals
	 * @param string $itemized the path to ingredients as an itemized list
	 */
	public function __construct($meals, $ingredients, $itemized)
	{
		$this->meals = $meals;
		$this->ingredients = $ingredients;
		$this->itemized = $itemized;
	}

	public function meals($meals)
	{
		$method = __FUNCTION__;

		$this->wrap(function () use ($method, $meals) {
			require $this->template($method);
		});
	}

	public function ingredients($ingredients)
	{
		$method = __FUNCTION__;

		$this->wrap(function () use ($method, $ingredients) {
			require $this->template($method);
		});
	}

	public function itemized($ingredients)
	{
		foreach ($ingredients as $ingredient => $quantity) {
			echo "{$quantity} x {$ingredient}\n";
		}
	}

	/**
	 * Wraps the given callback in HTML as an inset.
	 *
	 * @param callable $inset the callback to trigger during the inset
	 */
	private function wrap($inset)
	{
		require $this->template(__FUNCTION__);
	}

	/**
	 * Converts the given method name to a template path.
	 *
	 * @param string $method the method name to convert
	 * @return string the converted method name
	 */
	private function template($method)
	{
		$class = substr(__CLASS__, strlen(__NAMESPACE__) + 1);

		return dirname(__FILE__) . DIRECTORY_SEPARATOR
			. $class . DIRECTORY_SEPARATOR
			. "{$method}.php";
	}
}
