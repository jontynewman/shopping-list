<?php

namespace JontyNewman\ShoppingList\Applications;

use JontyNewman\ShoppingList\Presenter;
use JontyNewman\ShoppingList\Repository;
use ShrooPHP\Core\ArrayObject;
use ShrooPHP\Core\ReadOnlyArrayObject;
use ShrooPHP\Core\Request;
use ShrooPHP\Framework\Application;
use ShrooPHP\Framework\Request\Responses\Response;

/**
 * A web application for determining a shopping list for a given selection of
 * meals.
 */
class WebApplication
{
	/**
	 * the key required in order to specify a custom meals request path
	 */
	const MEALS = 1;

	/**
	 * the key required in order to specify a custom ingredients request path
	 */
	const INGREDIENTS = 2;

	/**
	 * the key required in order to specify a custom itemized request path
	 */
	const ITEMIZED = 3;

	/**
	 * the default request method for meals
	 */
	const MEALS_METHOD = 'GET';

	/**
	 * the default request method for ingredients
	 */
	const INGREDIENTS_METHOD = 'POST';

	/**
	 * the default request method for itemized ingredients
	 */
	const ITEMIZED_METHOD = 'POST';

	/**
	 * the default request path for meals
	 */
	const MEALS_PATH = '/';

	/**
	 * the default request path for ingredients
	 */
	const INGREDIENTS_PATH = '/shopping-list';

	/**
	 * the default request path for itemized ingredients
	 */
	const ITEMIZED_PATH = '/shopping-list.txt';

	/**
	 * Runs the application.
	 *
	 * @param \JontyNewman\ShoppingList\Repository $repository the repository
	 * to retrieve data from
	 * @param \JontyNewman\ShoppingList\Presenter $presenter the presenter to
	 * render data with
	 * @param array $paths customized request paths
	 * @param bool $test whether or not header manipulation should be skipped
	 * (for testing purposes)
	 */
	public function run(
		Repository $repository,
		Presenter $presenter,
		array $paths = null,
		$test = false
	) {

		$array = new ArrayObject(is_null($paths) ? array() : $paths);
		$meals_path = $array->get(self::MEALS, self::MEALS_PATH);
		$ingredients_path = $array->get(self::INGREDIENTS, self::INGREDIENTS_PATH);
		$itemized_path = $array->get(self::ITEMIZED, self::ITEMIZED_PATH);

		// GET /
		$meals = Response::callback(function () use ($repository, $presenter) {
			$presenter->meals($repository->meals());
		}, $test ? null : 'text/html');

		// POST /shopping-list
		$ingredients = function (Request $request) use ($repository, $presenter, $test) {
			$meals = $this->toArray($request->data(), 'meals');
			return $this->ingredients($meals, $repository, $presenter, $test);
		};

		// POST /shopping-list.txt
		$itemized = function (Request $request) use ($presenter, $test) {
			$ingredients = $this->toArray($request->data(), 'ingredients');
			return $this->itemized($ingredients, $presenter, $test);
		};

		$http404 = Response::string('Not Found', 'text/plain')
			->setCode(Response::HTTP_NOT_FOUND);

		$app = new Application;
		$app->match(self::MEALS_METHOD, $meals_path, $meals);
		$app->match(self::INGREDIENTS_METHOD, $ingredients_path, $ingredients);
		$app->match(self::ITEMIZED_METHOD, $itemized_path, $itemized);
		$app->push($http404);
		$app->run();
	}

	/**
	 * Converts the given array object and key to an array.
	 *
	 * @param \ShrooPHP\Core\ReadOnlyArrayObject $array the array object to
	 * convert
	 * @param scalar $key the key to convert
	 * @return array the converted array object
	 */
	private function toArray(ReadOnlyArrayObject $array, $key)
	{
		$meals = $array->filter($key, FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

		if (false === $meals) {
			$meals = array();
		}

		return $meals;
	}

	/**
	 * Converts the given meals, repository and presenter to ingredients as a
	 * response.
	 *
	 * @param array $meals the meals to convert
	 * @param \JontyNewman\ShoppingList\Repository $repository the repository
	 * to convert
	 * @param \JontyNewman\ShoppingList\Presenter $presenter the presenter to
	 * convert
	 * @param bool $test whether or not header manipulation should be skipped
	 * (for testing purposes)
	 * @return \ShrooPHP\Core\Request\Response the associated ingredients
	 */
	private function ingredients(
		array $meals,
		Repository $repository,
		Presenter $presenter,
		$test = false
	) {

		$callback = function () use ($meals, $repository, $presenter) {
			$presenter->ingredients($repository->ingredients($meals));
		};

		return Response::callback($callback, $test ? null : 'text/html');
	}

	/**
	 * Converts the given ingredients and presenter to an itemized list as a
	 * response.
	 *
	 * @param array $ingredients the ingredients to convert
	 * @param \JontyNewman\ShoppingList\Presenter $presenter the presenter to
	 * convert
	 * @param bool $test whether or not header manipulation should be skipped
	 * (for testing purposes)
	 * @return \ShrooPHP\Core\Request\Response the downloaded ingredients
	 */
	private function itemized(
		array $ingredients,
		Presenter $presenter,
		$test = false
	) {

		$callback = function () use ($ingredients, $presenter) {
			$presenter->itemized($ingredients);
		};

		$response = Response::callback($callback, $test ? null : 'text/plain');

		if (!$test)
			$response->addHeader('Content-Disposition', 'attachment');

		return $response;
	}
}
