<?php

namespace JontyNewman\ShoppingList;

/**
 * A presenter of various views within the application.
 */
interface Presenter
{
	/**
	 * Presents the given meals.
	 *
	 * @param \Traversable|array $meals the meals to present
	 */
	public function meals($meals);

	/**
	 * Presents the given meals, indexed by the relevant ingredient.
	 *
	 * @param \Traversable|array $ingredients the ingredients (and the
	 * respective meals) to present
	 */
	public function ingredients($ingredients);

	/**
	 * Presents the given quantities, indexed by their respective ingredient.
	 *
	 * @param \Traversable|array $ingredients the ingredients to present
	 */
	public function itemized($ingredients);
}

