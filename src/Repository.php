<?php

namespace JontyNewman\ShoppingList;

/**
 * An application for generating shopping lists based on specified meals.
 */
interface Repository
{
	/**
	 * Lists meals that may be selected in order to generate a shopping list.
	 *
	 * @return \Traversable|array meals that may be selected in order to
	 * generate a shopping list.
	 */
	public function meals();

	/**
	 * Generates a shopping list for the given meals.
	 *
	 * @param \Traversable|array $meals the meals to generate a shopping list
	 * for
	 * @return \Traversable|array the generated shopping list (a mapping of
	 * ingredients to meals)
	 */
	public function ingredients($meals);
}

