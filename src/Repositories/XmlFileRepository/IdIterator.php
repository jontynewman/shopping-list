<?php

namespace JontyNewman\ShoppingList\Repositories\XmlFileRepository;

use Iterator;
use LogicException;
use XMLReader;

/**
 * An iterator that extracts ID attributes from all elements with a specific
 * name at the current depth.
 */
class IdIterator implements Iterator
{
	/**
	 * @var \XMLReader the XML reader currently being pulled from
	 */
	private $xml;

	/**
	 * @var string the name of the elements whose IDs are being extracted
	 */
	private $name;

	/**
	 * @var int the current index
	 */
	private $index;

	/**
	 * @var bool whether or not the last read was successful
	 */
	private $read;

	/**
	 * @var int the depth at which iteration began
	 */
	private $depth;

	/**
	 * @var scalar the current ID
	 */
	private $current;

	/**
	 * Constructs an iterator for extracting ID attributes from all elements
	 * with the given name at the current depth.
	 *
	 * @param \XMLReader $xml the reader to extract IDs from
	 * @param string $name the name of the elements to extract IDs from
	 */
	public function __construct(XMLReader $xml, $name)
	{
		$this->xml = $xml;
		$this->name = $name;
	}

	public function current()
	{
		return $this->current;
	}

	public function key()
	{
		return $this->index;
	}

	public function next()
	{
		$this->index = is_null($this->index) ? 0 : $this->index + 1;
		$this->current = $this->toId();
	}

	public function rewind()
	{
		if (!is_null($this->index)) {
			throw new LogicException;
		}

		$this->read = true;
		$this->depth = $this->xml->depth;
		$this->next();
	}

	public function valid()
	{
		return $this->read && $this->xml->depth >= $this->depth;
	}

	/**
	 * Destructs the ID iterator by closing the XML reader.
	 */
	public function __destruct()
	{
		$this->xml->close();
	}

	/**
	 * Reads the next node and updates the iterator accordingly.
	 *
	 * @return bool whether or not the read was successful
	 */
	private function read()
	{
		$this->read = $this->xml->read();
		return $this->read;
	}

	/**
	 * Determines whether or not the current node is suitable for having its ID
	 * extracted.
	 *
	 * @return bool whether or not the current node is suitable for having its
	 * ID extracted
	 */
	private function matches()
	{
		return $this->xml->depth === $this->depth
			&& $this->xml->nodeType === XMLReader::ELEMENT
			&& $this->xml->name === $this->name;
	}

	/**
	 * Extracts the next ID.
	 *
	 * @return string|null the next ID (or NULL if it could not be extracted)
	 */
	private function toId()
	{
		$id = null;

		while (is_null($id) && $this->valid()) {

			// If the current node is not suitable, skip it.
			if (!$this->matches()) {
				$this->read();
				continue;
			}

			// Attempt to extract the ID.
			$id = $this->xml->getAttribute('id');

			// Advance to the following node so as to prime the next iteration.
			$this->read();
		}

		return $id;
	}
}
