<?php

namespace JontyNewman\ShoppingList\Repositories;

use EmptyIterator;
use JontyNewman\ShoppingList\Repository;
use JontyNewman\ShoppingList\Repositories\XmlFileRepository\IdIterator;
use XMLReader;

/**
 * An XML file repository.
 */
class XmlFileRepository implements Repository
{
	/**
	 * @var string the path to the current XML file
	 */
	private $path;

	/**
	 * Constructs a XML file repository.
	 *
	 * @param string $path the path to the XML file
	 */
	public function __construct($path)
	{
		$this->path = $path;
	}

	public function meals()
	{
		$meals = null;
		$deepened = false;
		$type = XMLReader::ELEMENT;
		$xml = $this->toXmlReader();

		while (is_null($meals) && $xml->read()) {

			if ($type === $xml->nodeType && 'meals' == $xml->name) {
				$deepened = $this->deepen($xml);
			}

			if ($deepened) {
				$meals = new IdIterator($xml, 'meal');
			}
		}

		return $meals ? $meals : new EmptyIterator;
	}

	public function ingredients($meals)
	{
		$meal = null;
		$ingredients = array();
		$xml = $this->toXmlReader();
		$keys = $this->toAssociativeArray($meals);
		$valid = $xml->read();

		while ($valid) {

			// If the current node is not an element, move to the following.
			if (XMLReader::ELEMENT !== $xml->nodeType) {
				$valid = $xml->read();
				continue;
			}

			$valid = $this->extract($xml, $ingredients, $keys, $meal);
		}

		$xml->close();

		return $ingredients;
	}

	/**
	 * Generates an XML reader.
	 *
	 * @return \XMLReader the generated XML reader
	 * @throws \JontyNewman\ShoppingList\Repositories\XmlFileRepositoryException
	 * failed to create XML reader
	 */
	private function toXmlReader()
	{
		$xml = new XMLReader;

		if (!$xml->open($this->path)) {
			throw new XmlFileRepositoryException($this->path);
		}

		return $xml;
	}

	/**
	 * Converts the given meals to an associative array.
	 *
	 * @param \Traversable|array $meals the meals to convert
	 * @return array the converted meals
	 */
	private function toAssociativeArray($meals) {

		$associative = array();

		foreach ($meals as $meal) {
			$associative[$meal] = null;
		}

		return $associative;
	}

	/**
	 * Extracts the ID of the ingredient from the given XML reader.
	 *
	 * @param \XMLReader $xml the XML reader to extract from
	 * @param array $keys the meals being searched for (as array keys)
	 * @param string $meal the meal being matched
	 * @return string|null the extracted ingredient (or NULL if the ingredient
	 * could not be extracted
	 */
	private function toIngredient(XMLReader $xml, array $keys, $meal)
	{
		$ingredient = null;

		if ('ingredient' === $xml->name && array_key_exists($meal, $keys)) {
			$ingredient = $xml->getAttribute('id');
		}

		return $ingredient;
	}

	/**
	 * Reads from the given XML reader until a greater depth is reached.
	 *
	 * @param \XMLReader $xml the reader to read from until a greater depth is
	 * reached
	 * @return bool the last return value of the read
	 */
	private function deepen(XMLReader $xml)
	{
		$depth = $xml->depth;
		$read = $xml->read();

		while ($read && $xml->depth <= $depth) {
			$read = $xml->read();
		}

		return $read;
	}

	/**
	 * Extracts data from the given XML reader according to the current depth.
	 *
	 * @param \XMLReader $xml the XML reader to extract data from
	 * @param array $ingredients the data to manipulate upon extraction
	 * @param array $keys the meals being searched for (as array keys)
	 * @param string $meal the last known meal (if any)
	 *
	 * @return bool whether or not the last read from the XML reader was
	 * successful
	 */
	private function extract(
		XMLReader $xml,
		array &$ingredients,
		array $keys,
		&$meal = null
	) {
		$valid = true;

		switch ($xml->depth) {
			case 0:
				$meal = null;
				if ('meals' === $xml->name) {
					$valid = $xml->read();
				} else {
					$valid = $xml->next();
				}
				break;
			case 1:
				$meal = null;
				if ('meal' === $xml->name) {
					$meal = $xml->getAttribute('id');
				}
				$valid = $xml->read();
				break;
			case 2:
				if ('ingredients' === $xml->name) {
					$valid = $xml->read();
				} else {
					$valid = $xml->next();
				}
				break;
			case 3:
				$ingredient = null;

				if (!is_null($meal)) {
					$ingredient = $this->toIngredient($xml, $keys, $meal);
				}

				if (!is_null($ingredient)) {
					$this->manipulate($ingredients, $ingredient, $meal);
				}
				// Fallthrough...
			default:
				$valid = $xml->read();
				break;
		}

		return $valid;
	}

	/**
	 * Manipulates the given ingredients so as to add the given meal to the
	 * given ingredient.
	 *
	 * @param array $ingredients the ingredients to manipulate
	 * @param scalar $ingredient the ingredient to add the meal to
	 * @param mixed $meal the meal to add to the ingredient
	 */
	private function manipulate(array &$ingredients, $ingredient, $meal)
	{
		if (isset($ingredients[$ingredient])) {
			$ingredients[$ingredient][] = $meal;
		} else {
			$ingredients[$ingredient] = array($meal);
		}
	}
}

