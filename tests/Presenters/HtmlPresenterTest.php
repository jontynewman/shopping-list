<?php

namespace JontyNewman\ShoppingList\Tests\Presenters;

use JontyNewman\ShoppingList\Presenters\HtmlPresenter;
use PHPUnit\Framework\TestCase;

class HtmlPresenterTest extends TestCase
{
	/**
	 * @var \JontyNewman\ShoppingList\Presenters\HtmlPresenter the presenter
	 * currently being tested
	 */
	private $presenter;

	/**
	 * @var array the meals currently being tested
	 */
	private $meals;

	/**
	 * @var array the ingredients currently being tested
	 */
	private $ingredients;

	/**
	 * @var array the downloaded ingredients currently being tested
	 */
	private $download;

	/**
	 * Sets up each test by initializing the meals, the (downloaded)
	 * ingredients, and the presenter.
	 */
	public function setUp()
	{
		$this->presenter = new HtmlPresenter('', '', '');
		$this->meals = array('A Meal', 'Another Meal', 'Yet Another Meal');
		$this->ingredients = array(
			         'An Ingredient' => $this->meals,
			    'Another Ingredient' => $this->meals,
			'Yet Another Ingredient' => $this->meals
		);
		$this->download = array(
			         'An Ingredient' => 1,
			    'Another Ingredient' => 2,
			'Yet Another Ingredient' => 3
		);
	}

	/**
	 * Asserts that the output of the meals is as expected.
	 */
	public function testMeals()
	{
		$this->assertTrue(ob_start());

		$this->presenter->meals($this->meals);

		$output = ob_get_clean();

		$this->assertNotFalse($output);
		$this->assertNotEquals('', $output);
	}

	/**
	 * Asserts that the output of the ingredients is as expected.
	 */
	public function testIngredients()
	{
		$this->assertTrue(ob_start());

		$this->presenter->ingredients($this->ingredients);

		$output = ob_get_clean();

		$this->assertNotFalse($output);
		$this->assertNotEquals('', $output);
	}

	/**
	 * Asserts that the output of the itemized ingredients is as expected.
	 */
	public function testItemized()
	{
		$this->assertTrue(ob_start());

		$this->presenter->itemized($this->download);

		$output = ob_get_clean();

		$this->assertNotFalse($output);
		$this->assertNotEquals('', $output);
	}
}
