<?php

namespace JontyNewman\ShoppingList\Tests\Applications\WebApplicationTest;

use JontyNewman\ShoppingList\Repository as IRepository;

/**
 * A repository that increments certain properties.
 */
class Repository implements IRepository
{

	/**
	 * @var int the number of times that meals have been retrieved from the
	 * repository
	 */
	public $meals = 0;

	/**
	 * @var int the number of times that ingredients have been retrieved from
	 * the repository
	 */
	public $ingredients = 0;

	/**
	 * @var type
	 */
	private $test;

	public function meals()
	{
		$this->meals++;
		return array();
	}

	public function ingredients($meals)
	{
		$this->ingredients++;
		return array();
	}

}