<?php

namespace JontyNewman\ShoppingList\Tests\Applications\WebApplicationTest;

use JontyNewman\ShoppingList\Presenter as IPresenter;

/**
 * A presenter that increments certain properties.
 */
class Presenter implements IPresenter
{

	/**
	 * @var int the number of times that meals have been presented by the
	 * presenter
	 */
	public $meals = 0;

	/**
	 * @var int the number of times that ingredients have been presented by the
	 * presenter
	 */
	public $ingredients = 0;

	/**
	 * @var int the number of times that items have been presented by the
	 * presenter
	 */
	public $itemized = 0;

	public function meals($meals)
	{
		$this->meals++;
	}

	public function ingredients($ingredients)
	{
		$this->ingredients++;
	}

	public function itemized($ingredients)
	{
		$this->itemized++;
	}

}
