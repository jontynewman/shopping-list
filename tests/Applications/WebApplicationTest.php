<?php

namespace JontyNewman\ShoppingList\Tests\Applications;

use JontyNewman\ShoppingList\Applications\WebApplication;
use JontyNewman\ShoppingList\Tests\Applications\WebApplicationTest\Presenter;
use JontyNewman\ShoppingList\Tests\Applications\WebApplicationTest\Repository;
use PHPUnit\Framework\TestCase;

class WebApplicationTest extends TestCase
{
	/**
	 * @var array a cache of execution environment variables
	 */
	private static $server;

	/**
	 * @var array a cache of query parameters
	 */
	private static $get;

	/**
	 * @var array a cache of body data
	 */
	private static $post;

	/**
	 * @var \JontyNewman\ShoppingList\Applications\WebApplication the
	 * application currently being tested.
	 */
	private $application;

	/**
	 * @var \JontyNewman\ShoppingList\Applications\WebApplication\Repository
	 * the repository being used to retrieve data
	 */
	private $repository;

	/**
	 * @var \JontyNewman\ShoppingList\Applications\WebApplication\Presenter
	 * the presenter being used to present data
	 */
	private $presenter;

	/**
	 * Sets up the test case by caching the execution environment variables,
	 * the query parameters and the body data.
	 */
	public static function setUpBeforeClass()
	{
		self::$server = $_SERVER;
		self::$get    = $_GET;
		self::$post   = $_POST;
	}

	/**
	 * Tears down the test case by reassigning the cached environment
	 * variables, the cached query parameters and the cached body data.
	 */
	public static function tearDownAfterClass()
	{
		$_SERVER = self::$server;
		$_GET    = self::$get;
		$_POST   = self::$post;
	}

	/**
	 * Sets up each test be initializing the application.
	 */
	public function setUp()
	{
		$this->repository = new Repository;
		$this->presenter = new Presenter;
		$this->application = new WebApplication;
	}

	/**
	 * Asserts that a request for all meals via the default request path
	 * behaves as expected.
	 */
	public function testDefaultMealsPath()
	{
		$this->assertMeals();
	}

	/**
	 * Asserts that a request for ingredients of certain meals via the default
	 * request path behaves as expected.
	 */
	public function testDefaultIngredientsPath()
	{
		$this->assertIngredients();
	}

	/**
	 * Asserts that a request to itemize certain ingredients via the default
	 * request path behaves as expected.
	 */
	public function testDefaultItemizedPath()
	{
		$this->assertItemized();
	}

	/**
	 * Asserts that a request for all meals via a custom request path behaves
	 * as expected.
	 */
	public function testCustomMealsPath()
	{
		$this->assertMeals('custom-meals-path');
	}

	/**
	 * Asserts that a request for ingredients of certain meals via a custom
	 * request path behaves as expected.
	 */
	public function testCustomIngredientsPath()
	{
		$this->assertIngredients('custom-ingredients-path');
	}

	/**
	 * Asserts that a request to itemize certain ingredients via a custom
	 * request path behaves as expected.
	 */
	public function testCustomItemizedPath()
	{
		$this->assertItemized('custom-itemized-path');
	}

	/**
	 * Asserts that a request for all meals behaves as expected.
	 *
	 * @param string|null $path a customized request path
	 */
	private function assertMeals($path = null)
	{
		$default = WebApplication::MEALS_PATH;
		$_SERVER['REQUEST_METHOD'] = WebApplication::MEALS_METHOD;
		$_SERVER['REQUEST_URI'] = is_null($path) ? $default : $path;

		$this->runApplication(array(WebApplication::MEALS => $path));

		$this->assertEquals(1, $this->repository->meals);
		$this->assertEquals(1, $this->presenter->meals);
	}

	/**
	 * Asserts that a request for ingredients of certain meals behaves as
	 * expected.
	 *
	 * @param string|null $path a customized request path
	 */
	public function assertIngredients($path = null)
	{
		$default = WebApplication::INGREDIENTS_PATH;
		$_SERVER['REQUEST_METHOD'] = WebApplication::INGREDIENTS_METHOD;
		$_SERVER['REQUEST_URI'] = is_null($path) ? $default : $path;

		$this->runApplication(array(WebApplication::INGREDIENTS => $path));

		$this->assertEquals(1, $this->repository->ingredients);
		$this->assertEquals(1, $this->presenter->ingredients);
	}

	/**
	 * Asserts that a request to itemize certain ingredients behaves as expected.
	 *
	 * @param string|null $path a customized request path
	 */
	public function assertItemized($path = null)
	{
		$default = WebApplication::ITEMIZED_PATH;
		$_SERVER['REQUEST_METHOD'] = WebApplication::ITEMIZED_METHOD;
		$_SERVER['REQUEST_URI'] = is_null($path) ? $default : $path;

		$this->runApplication(array(WebApplication::ITEMIZED => $path));

		$this->assertEquals(1, $this->presenter->itemized);
	}

	/**
	 * Runs the application being tested.
	 *
	 * @param array|null $paths customized request paths
	 */
	public function runApplication(array $paths = null)
	{
		$this->application->run($this->repository, $this->presenter, $paths, true);
	}

}
