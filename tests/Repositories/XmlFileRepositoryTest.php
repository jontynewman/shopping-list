<?php

namespace JontyNewman\ShoppingList\Tests\Repositories;

use JontyNewman\ShoppingList\Repositories\XmlFileRepository;
use PHPUnit\Framework\TestCase;

class XmlFileRepositoryTest extends TestCase
{
	/**
	 * the XML being used for testing purposes
	 */
	const XML = <<<'EOF'
<?xml version="1.0" encoding="UTF-8"?>
<meals>
	<meal id="A Meal">
		<ingredients>
			<ingredient id="An Ingredient" />
			<ingredient id="Another Ingredient" />
		</ingredients>
	</meal>
	<meal id="Another Meal">
		<ingredients>
			<ingredient id="An Ingredient" />
			<ingredient id="Yet Another Ingredient" />
		</ingredients>
	</meal>
	<meal id="Yet Another Meal">
		<ingredients>
			<ingredient id="Another Ingredient" />
			<ingredient id="Yet Another Ingredient" />
		</ingredients>
	</meal>
</meals>
EOF;

	/**
	 * @var string the path to the XML file currently being used for testing
	 * purposes
	 */
	private static $path;

	/**
	 * @var array the data being stored in the XML file currently being used
	 * for testing purposes
	 */
	private static $data;

	/**
	 * @var \JontyNewman\ShoppingList\Repositories\XmlFileRepository the
	 * repository currently being tested
	 */
	private $repo;

	/**
	 * Sets up the test case by initializing the path, the data, and the XML
	 * file.
	 */
	public static function setUpBeforeClass()
	{
		self::$path = 'tests' . DIRECTORY_SEPARATOR . 'meals.xml';

		self::$data = array(
			'A Meal' => array(
				'An Ingredient',
				'Another Ingredient',
			),
			'Another Meal' => array(
				'An Ingredient',
				'Yet Another Ingredient',
			),
			'Yet Another Meal' => array(
				'Another Ingredient',
				'Yet Another Ingredient',
			),
		);

		$xml = fopen(self::$path, 'w+');
		fwrite($xml, self::XML);
		fclose($xml);
	}

	/**
	 * Sets up each test by initializing the repository.
	 */
	public function setUp()
	{
		$this->repo = new XmlFileRepository(self::$path);
	}

	/**
	 * Asserts that indexing lists all meals within the XML file.
	 */
	public function testMeals()
	{
		$meals = iterator_to_array($this->repo->meals());
		$this->assertEquals(array_keys(self::$data), $meals);
	}

	/**
	 * Asserts that generation lists all ingredients associated with the
	 * specified meals.
	 */
	public function testIngredients()
	{
		$meals = array('Another Meal', 'Yet Another Meal');
		$ingredients = array(
			'An Ingredient' => array(
				'Another Meal',
			),
			'Another Ingredient' => array(
				'Yet Another Meal',
			),
			'Yet Another Ingredient' => array(
				'Another Meal',
				'Yet Another Meal',
			),
		);

		$this->assertEquals($ingredients, $this->repo->ingredients($meals));
	}
}
