<?php

namespace JontyNewman\ShoppingList\Tests\Repositories\XmlFileRepository;

use JontyNewman\ShoppingList\Repositories\XmlFileRepository\IdIterator;
use PHPUnit\Framework\TestCase;
use XMLReader;

class IdIteratorTest extends TestCase
{
	const XML = <<<'EOF'
<?xml version="1.0" encoding="UTF-8"?>
<elements>
	Text Node
	<element id="first" />
	<element id="second">
		<child id="ignore1" />
		<child id="ignore2">
			<subchild>
			</subchild>
		</child>
	</element>
	<ignore></ignore>
	<ignore id="ignore3">
		Insignificant Text Node
	</ignore>
	<ignore id="ignore4" />
	<element />
	Another Text Node
	<element id="third">
		Yet Another Text Node
	</element>
</elements>
EOF;

	/**
	 * Asserts that the extraction of IDs behaves as expected.
	 */
	public function testIdIterator()
	{
		$expected = array('first', 'second', 'third');
		$xml = new XMLReader;
		$iterator = new IdIterator($xml, 'element');

		$xml->XML(self::XML);

		while ($xml->depth <= 0) {
			$xml->read();
		}

		$this->assertEquals($expected, iterator_to_array($iterator));
	}
}